﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ItemType = ScriptableWheelItem.ItemType;

public class WheelItemLarge : MonoBehaviour
{
    public GameItem Item { get; set; }
    
    [Header("Values")] 
    [SerializeField][Range(0, 2f)] private float scaleDuration;
    
    [Header("Dependencies")] 
    [SerializeField] private Image frames;
    [SerializeField] private Image icon;
    [SerializeField] private Image amountPanel;
    [SerializeField] private TMP_Text amountText;
    [SerializeField] private TMP_Text itemName;
    [SerializeField] private TMP_Text battleItemName;
    [SerializeField] private TMP_Text battleItemRarity;

    public bool ShowStackAmount { get; set; }
    
    private GameSystem _system;
    
    private void OnEnable()
    {
        SetDependencyValues();
        ZoomInAnimation();
    }

    private void Start()
    {
        _system = GameSystem.Instance;
        _system.OnRewardInteractionEnded += () => gameObject.SetActive(false);
    }

    private void SetDependencyValues()
    {
        frames.color = Item.FrameColor;
        icon.sprite = Item.Sprite;

        switch (Item.Type)
        {
            case ItemType.BattleItem:
                amountPanel.gameObject.SetActive(false);
                battleItemName.text = Item.ItemName;
                itemName.text = Item.BattleItemName;
                battleItemRarity.text = Item.Rarity.ToString();
                break;
            
            case ItemType.Death:
                amountPanel.gameObject.SetActive(false);
                battleItemName.text = "";
                itemName.text = "";
                battleItemRarity.text = "";
                GameSystem.Instance.EndRewardInteraction();
                break;
            
            default:
                amountPanel.gameObject.SetActive(true);
                amountPanel.color = Item.FrameColor;
                var amount = ShowStackAmount ? Item.StackAmount : Item.DropAmount;
                amountText.text = amount.ToString("#,##0");
                battleItemName.text = "";
                itemName.text = Item.ItemName;
                battleItemRarity.text = "";
                break;
        }

    }

    private void ZoomInAnimation()
    {
        var scale = transform.localScale;
        transform.localScale = Vector3.zero;

        transform.DOScale(scale, scaleDuration)
            .SetEase(Ease.OutBack);
    }
}

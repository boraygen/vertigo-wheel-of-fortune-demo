using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ZoneItem : MonoBehaviour
{
    [Header("Resources")]
    [SerializeField] private Sprite normalZone;
    [SerializeField] private Sprite safeZone;
    [SerializeField] private Sprite goldenZone;
    
    [Header("Values")] 
    [SerializeField] private float fontSize;
    
    [Header("Dependencies")]
    [SerializeField] private Image image;
    [SerializeField] private TMP_Text tmpText;

    public int LevelNumber
    {
        get => _levelNumber;
        set
        {
            _levelNumber = value;
            SetDependencyValues();
        }
    }
    
    private int _levelNumber;

    private void OnValidate()
    {
        SetDependencyValues();
    }

    private void Start()
    {
        SetDependencyValues();
    }

    private void SetDependencyValues()
    {
        SetLevelNumberText();
        SetBackgroundSprite();
    }

    private void SetLevelNumberText()
    {
        tmpText.text = LevelNumber.ToString();
        tmpText.fontSize = fontSize;
    }

    private void SetBackgroundSprite()
    {
        int number = int.Parse(tmpText.text);
        var zoneType = GameSystem.GetZoneType(number);
        
        image.sprite = zoneType switch
        {
            GameSystem.ZoneType.Silver => safeZone,
            GameSystem.ZoneType.Gold => goldenZone,
            _ => normalZone
        };
    }
}

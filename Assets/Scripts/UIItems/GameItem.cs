﻿using UnityEngine;
using ItemType = ScriptableWheelItem.ItemType;
using Rarity = ScriptableWheelItem.Rarity;

public class GameItem
{
    public int Id { get; private set; }
    public string ItemName => _item.itemName;
    public Sprite Sprite => _item.sprite;
    public float DropRate => _item.dropRate;
    public ItemType Type => _item.type;
    public string BattleItemName => _item.battleItemName;
    public Rarity Rarity => _item.rarity;
    public Color FrameColor { get; set; }
    public bool IsDropped { get; set; }

    public int DropAmount => _item.Amount;
    public int StackAmount { get; set; }
    public int PuzzlePieceGiven
    {
        get => _item.puzzlePieceGiven + _stack;
        set => _stack = value;
    }

    private readonly ScriptableWheelItem _item;
    private int _stack;

    public GameItem(int id, ScriptableWheelItem item)
    {
        _item = item;
        FrameColor = _item.frameColor;
        Id = id;
    }
}

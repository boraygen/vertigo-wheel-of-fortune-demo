﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ItemType = ScriptableWheelItem.ItemType;

public class LootBoxItem : MonoBehaviour
{
    [Header("Resources")]
    [SerializeField] private Sprite puzzlePieceSprite;

    [Header("Values")]
    [SerializeField][Range(0, 3f)] private float valueUpdateDuration;
    [SerializeField][Range(1, 3.5f)]  private float rewardInteractionDuration;
    
        
    [Header("Dependencies")]
    [SerializeField] private Image image;
    [SerializeField] private TMP_Text tmpText;

    private GameItem Item { get; set; }
    public int Id => Item.Id;
    public ItemType Type => Item.Type;

    public int PuzzlePieceGiven
    {
        get => Item.PuzzlePieceGiven;
        set => Item.PuzzlePieceGiven = value;
    }
    public int Amount
    {
        get => _amount;
        private set
        {
            _amount = value;
            // print($"lootBoxItem Amount: {Amount}");
            UpdateAmountText();
        }
    }

    private GameSystem _system; 
    private int _amount;
    private bool _isBattleItem;

    private void Start()
    {
        _system = GameSystem.Instance;
    }

    public void Init(GameItem item)
    {
        Item = item;
        _isBattleItem = Item.Type == ItemType.BattleItem;
        
        Sprite sprite = _isBattleItem ? puzzlePieceSprite : Item.Sprite;
        // int amount = _isBattleItem ? Item.puzzlePieceGiven : Item.Amount;
        
        SetImage(sprite);
        UpdateAmountValue(item);
    }

    private void SetImage(Sprite sprite)
    {
        image.sprite = sprite;
    }

    public void UpdateAmountValue(GameItem item)
    {
        // these processes may be done with ".DOCounter" if pro version is available
        int targetValue = _isBattleItem ? item.PuzzlePieceGiven : item.DropAmount;
        targetValue += Amount;
        
        DOTween.To(
            () => Amount,
            x => Amount = x,
            targetValue,
            valueUpdateDuration
        ).OnStart(() => _system.EndRewardInteraction());
    }

    private void UpdateAmountText()
    {
        tmpText.text = Amount.ToString("#,##0");
    }
}

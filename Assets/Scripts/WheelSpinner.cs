using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;
using ItemType = ScriptableWheelItem.ItemType;

public class WheelSpinner : MonoBehaviour
{
    [Header("Values")]
    [SerializeField] private float minSpin;
    [SerializeField] private float maxSpin;
    
    [SerializeField] private bool resetBeforeSpinning;
    [SerializeField][Range(0, 2f)] private float rotationResetDuration;
    [SerializeField][Range(0, 5f)] private float spinDuration;
    
    [Header("Dependencies")] 
    [SerializeField] private Transform wheel;
    [SerializeField] private WheelItemLarge wheelItemLarge;
    
    private GameSystem _system;
    private PlayerLoot _loot;
    private WheelItemSpawner _spawner;
    private int _pickedSliceIndex;
    private GameItem _pickedItem;
    
    private void Start()
    {
        _system = GameSystem.Instance;
        _loot = PlayerLoot.Instance;
        _spawner = GetComponent<WheelItemSpawner>();
        _system.OnWheelSpinStarted += Spin;
        _system.OnWheelSpinEnded += PickSliceIndex;
        _system.OnWheelSpinEnded += PickItemBasedOnSlice;
        _system.OnWheelSpinEnded += HandlePlayerLoot;
        // _system.OnWheelSpinEnded += Debug;
    }

    private void Spin()
    {
        var randomSpin = Random.Range(minSpin, maxSpin);
        float randomSpinAngle = 360f * randomSpin;

        Vector3 targetPos = resetBeforeSpinning ? Vector3.zero : wheel.localRotation.eulerAngles;
        float duration = resetBeforeSpinning ? rotationResetDuration : 0;

        wheel.DOLocalRotate(targetPos, duration)
            .OnKill(() =>
            {
                wheel.DOLocalRotate(new Vector3(0, 0, randomSpinAngle), spinDuration, RotateMode.FastBeyond360)
                    .SetEase(Ease.OutQuad)
                    .OnKill(_system.EndWheelSpin);
            });
    }

    private void PickSliceIndex()
    {
        float angle = wheel.localRotation.eulerAngles.z;

        if (angle < 0)
        {
            angle += 360f;
        }

        int slice = wheel.childCount;
        float sliceAngle = 360f / slice;
        
        for (int i = 0; i < slice; i++)
        {
            float lowerLimit = sliceAngle * i;
            float upperLimit = sliceAngle * (i + 1);
            
            if (angle >= lowerLimit && angle < upperLimit)
            {
                // _slicePicked = wheel.GetChild(i).gameObject;
                _pickedSliceIndex = i;
                break;
            }
        }
    }

    private void PickItemBasedOnSlice()
    { 
        _pickedItem = _spawner.ItemsToSpawn[_pickedSliceIndex];
        wheelItemLarge.Item = _pickedItem;
        wheelItemLarge.gameObject.SetActive(true);
    }
    
    private void HandlePlayerLoot()
    {
        if (_pickedItem.Type == ItemType.Death)
        {
            Invoke(nameof(ResetWheel), 2f);
        }
        else
        {
            if (_pickedItem.Type == ItemType.BattleItem) // to prevent the same item from spawning again
            {
                _spawner.RemoveItemFromGameItems(_pickedItem);
            }
            _loot.AddItem(_pickedItem);
        }
    }

    private void ResetWheel()
    {
        _system.ResetWheel();

    }
}

using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class ZoneScrollSnapper : MonoBehaviour
{
    [Header("Values")]
    [SerializeField][Range(0, 2f)] private float snapAnimationTime;
    
    [Header("Dependencies")] 
    [SerializeField] private RectTransform contentPanel;

    private GameSystem _events;
    private RectTransform _snapTarget;
    private ScrollRect _scrollRect;
    
    private void Start()
    {
        _events = GameSystem.Instance;
        _scrollRect = GetComponent<ScrollRect>();
        
        _events.OnSpawnCompleted += SetSnapTarget;
        _events.OnLevelZoneChanged += SetSnapTarget;
        SetSnapTarget();
    }

    // private void Update()
    // {
    //     if (Input.GetKeyDown(KeyCode.O))
    //     {
    //         SnapTo(_snapTarget);
    //     }
    // }

    private void SetSnapTarget()
    {
        int levelIndex = _events.CurrentLevelZone - 1;
        
        try
        {
            _snapTarget = contentPanel.GetChild(levelIndex).GetComponent<RectTransform>();
            SnapTo(_snapTarget);
        }
        catch (Exception e)
        {
            // Console.WriteLine(e);
            // throw;
        }
    }

    private void SnapTo(RectTransform target)
    {
        Canvas.ForceUpdateCanvases();

        var contentPos = (Vector2)_scrollRect.transform.InverseTransformPoint(contentPanel.position);
        var targetPos = (Vector2)_scrollRect.transform.InverseTransformPoint(target.position);
        Vector2 targetSnapPos = contentPos - targetPos;
        
        contentPanel.DOAnchorPos(targetSnapPos, snapAnimationTime);

        // contentPanel.anchoredPosition =
        //     (Vector2)_scrollRect.transform.InverseTransformPoint(contentPanel.position)
        //     - (Vector2)_scrollRect.transform.InverseTransformPoint(target.position);
    }
}

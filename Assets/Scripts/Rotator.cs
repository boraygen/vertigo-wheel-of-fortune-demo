using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UIElements;

public class Rotator : MonoBehaviour
{
    [SerializeField][Range(0, 180f)] private float rotationAnglePerSecond;
    [SerializeField] private bool isClockWise;
    
    private void Start()
    {
        Rotate();
    }

    private void Rotate()
    {
        var target = transform.rotation.eulerAngles;
        int factor = isClockWise ? -1 : 1;
        
        transform.DORotate(target + new Vector3(0, 0, rotationAnglePerSecond * factor), 1)
            .SetEase(Ease.Linear)
            .OnKill(Rotate);
    }
}

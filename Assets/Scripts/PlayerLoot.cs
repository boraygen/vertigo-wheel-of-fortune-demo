using System.Collections.Generic;
using UnityEngine;
using ItemType = ScriptableWheelItem.ItemType;

public class PlayerLoot : MonoBehaviour
{
    public static PlayerLoot Instance;
    
    public List<GameItem> PlayerItems { get; private set; } = new();
    // [field: SerializeField] public int PuzzlePieces { get; private set; } = 0; // debug

    // public Action OnNewLootAdded;
    // public Action<ScriptableWheelItem> OnLootUpdated;
    
    private GameSystem _system;
    private LootBoxPanelItemController _lootBox;
    
    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _system = GameSystem.Instance;
        _lootBox = LootBoxPanelItemController.Instance;
        _system.OnWheelReset += ResetLoot;
    }

    public void AddItem(GameItem newItem)
    {
        if (newItem.Type == ItemType.BattleItem)
        {
            PlayerItems.Add(newItem);
            _lootBox.AddNewLoot(newItem);
        }
        else
        {
            foreach (var playerItem in PlayerItems)
            {
                if (newItem.Id == playerItem.Id)
                {
                    // UpdateLoot(item);
                    playerItem.StackAmount += newItem.DropAmount;
                    // print($"playerLoot playerItem StackAmount: {playerItem.StackAmount}");

                    _lootBox.UpdateExistingLoot(newItem);
                    return;
                }
            }
            
            PlayerItems.Add(newItem);
            _lootBox.AddNewLoot(newItem);
        }
    }

    private void ResetLoot()
    {
        PlayerItems.Clear();
        // PuzzlePieces = 0;
        _lootBox.ResetLoot();
    }

    // private void AddNewLoot() => OnNewLootAdded?.Invoke();
    // private void UpdateLoot(ScriptableWheelItem item) => OnLootUpdated?.Invoke(ScriptableWheelItem item);
}

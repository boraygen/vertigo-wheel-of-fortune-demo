using System;
using System.Globalization;
using System.Threading;
using UnityEngine;

public class GameSystem : MonoBehaviour
{
    public static GameSystem Instance;

    // [SerializeField] private int levelZone;
    [SerializeField] private float rewardInteractionTime;
    
    public enum ZoneType
    {
        Normal,
        Silver,
        Gold,
    }
    
    public bool IsSpinning { get; set; }
    public int CurrentLevelZone
    {
        get => _currentLevelZone;
        private set
        {
            _currentLevelZone = value;
            ChangeLevelZone();
        }
    }

    public Action OnSpawnCompleted;
    public Action OnLevelZoneChanged;
    public Action OnWheelSpinStarted;
    public Action OnWheelSpinEnded;
    public Action OnRewardInteractionEnded;
    public Action OnWheelReset;

    // private bool _isSpinning;
    private int _currentLevelZone;
    private const int InitialLevel = 1;

    private void Awake()
    {
        Instance = this;
        CurrentLevelZone = InitialLevel;
        Thread.CurrentThread.CurrentCulture = new CultureInfo("en-GB");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-GB");
    }

    private void Start()
    {
        OnWheelSpinStarted += () => IsSpinning = true;
        OnWheelSpinEnded += () => IsSpinning = false;
        OnRewardInteractionEnded += IncreaseLevelZone;
        OnWheelReset += ResetLevelZone;
    }

    // private void OnValidate()
    // {
    //     CurrentLevelZone = levelZone;
    // }

    private void IncreaseLevelZone()
    {
        CurrentLevelZone++;
    }

    private void ResetLevelZone()
    {
        CurrentLevelZone = InitialLevel;
    }

    public static ZoneType GetZoneType(int level)
    {
        if (level % 30 == 0)
        {
            return ZoneType.Gold;
        }
        
        if (level == 1 || level % 5 == 0)
        {
            return ZoneType.Silver;
        }
        
        return ZoneType.Normal;
    }

    public void CompleteSpawn() => OnSpawnCompleted?.Invoke();
    public void ChangeLevelZone() => OnLevelZoneChanged?.Invoke();
    public void StartWheelSpin()
    {
        if (!IsSpinning)
        {
            OnWheelSpinStarted?.Invoke();   
        }
    }
    public void EndWheelSpin() => OnWheelSpinEnded?.Invoke();
    public void ResetWheel() => OnWheelReset?.Invoke();
    public void EndRewardInteraction() => Invoke(nameof(EndRewardInteractionDelayed), rewardInteractionTime);
    private void EndRewardInteractionDelayed() => OnRewardInteractionEnded?.Invoke();
}

﻿using UnityEngine;

    [CreateAssetMenu(fileName = "Wheel Item", menuName = "Wheel Item", order = 0)]
    public class ScriptableWheelItem : ScriptableObject
    {
        [Header("Base")]
        public string itemName;
        public Sprite sprite;
        public float dropRate;

        public ItemType type;
        
        [SerializeField] private int amount; // for expendable
        [Range(0, 1f)] public float amountIncreasedPerLevelRatio;
        public Color frameColor; // for item
        public Rarity rarity; // for item
        public string battleItemName; // for item
        public int puzzlePieceGiven;  // for item

        public int Amount => amount + Mathf.RoundToInt(amount * amountIncreasedPerLevelRatio * (GameSystem.Instance.CurrentLevelZone - 1));

        
        public enum ItemType
        {
            None,
            Expendable,
            BattleItem,
            // Chest,
            Death,
        }
        
        public enum Rarity
        {
            Common,
            Rare,
            Epic,
            Legendary,
        }

    }

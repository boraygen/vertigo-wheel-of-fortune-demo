using UnityEngine;
using ItemType = ScriptableWheelItem.ItemType;

public class LootBoxPanelItemController : MonoBehaviour
{
    public static LootBoxPanelItemController Instance;
    
    [Header("Resources")]
    [SerializeField] private LootBoxItem prefab;
    
    [Header("Dependencies")]
    [SerializeField] private Transform content;

    private bool _noBattleItemFound = true;
    
    private void Awake()
    {
        Instance = this;
    }

    public void AddNewLoot(GameItem item)
    {
        if (item.Type == ItemType.BattleItem)
        {
            if (_noBattleItemFound)
            {
                _noBattleItemFound = false;
                LootBoxItem lootItem = Instantiate(prefab, content);
                lootItem.Init(item);
            }
            else
            {
                UpdateBattleItemLoot(item);
            }
        }
        else
        {
            LootBoxItem lootItem = Instantiate(prefab, content);
            item.StackAmount += item.DropAmount;
            lootItem.Init(item);
        }
        
    }

    private void UpdateBattleItemLoot(GameItem item)
    {
        foreach (Transform child in content.transform)
        {
            LootBoxItem lootBoxItem = child.GetComponent<LootBoxItem>();

            if (lootBoxItem.Type == ItemType.BattleItem)
            {
                lootBoxItem.UpdateAmountValue(item);
                // lootBoxItem.PuzzlePieceGiven += item.PuzzlePieceGiven;
                // GameSystem.Instance.EndRewardInteraction();
                // lootBoxItem.UpdateAmountValue(item); // ??
            }
        }
    }

    public void UpdateExistingLoot(GameItem item)
    {
        foreach (Transform child in content.transform)
        {
            LootBoxItem lootBoxItem = child.GetComponent<LootBoxItem>();

            if (lootBoxItem.Id == item.Id)
            {
                lootBoxItem.UpdateAmountValue(item);
                break;
            }
        }
    }

    public void ResetLoot()
    {
        foreach (Transform child in content.transform)
        {
            Destroy(child.gameObject);
        }

        _noBattleItemFound = true;
    }
    
}

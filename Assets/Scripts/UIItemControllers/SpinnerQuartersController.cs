using UnityEngine;

public class SpinnerQuartersController : MonoBehaviour
{
    [Header("Dependencies")]
    [SerializeField] private RectTransform wheel;

    private void Start()
    {
        SetWheelWidthAndHeight();
    }

    private void SetWheelWidthAndHeight()
    {
        var reference = GetComponent<RectTransform>().sizeDelta;

        foreach (Transform child in wheel.transform)
        {
            var childRect = child.GetComponent<RectTransform>();
            // childRect.sizeDelta = reference * 0.5f;
            childRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, reference.x * 0.5f);
            childRect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, reference.y * 0.5f);
        }
    }
}

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using ItemType = ScriptableWheelItem.ItemType;
using Rarity = ScriptableWheelItem.Rarity;

public class WheelItemSpawner : MonoBehaviour
{
    [SerializeField] [Tooltip("Remove repetitive items from Game Items.\n\nThis may increase scene load time, if enabled")] private bool removeRepetitiveItems;
    [SerializeField] private List<ScriptableWheelItem> scriptableGameItems;
    [SerializeField] private ScriptableWheelItem deathItem;
    
    [Header("Battle Item Frame Colors")]
    [SerializeField] private Color commonColor;
    [SerializeField] private Color rareColor;
    [SerializeField] private Color epicColor;
    [SerializeField] private Color legendaryColor;

    public List<GameItem> ItemsToSpawn { get; private set; } = new();
    
    private GameSystem _system;
    private GameItem _deathItem;
    private List<GameItem> _gameItems = new();
    
    private void Start()
    {
        _system = GameSystem.Instance;
        
        if (removeRepetitiveItems)
        {
            RemoveRepetitiveItems();
        }

        SetGameItems();
        SetFrameColors();

        _system.OnWheelReset += ResetItems;
        _system.OnWheelSpinStarted += () => SetRandomItemsToSpawn(4);
        _system.OnWheelSpinStarted += AddBombToItemsToSpawn;
    }
    
    public void RemoveItemFromGameItems(GameItem item)
    {
        _gameItems.First(i => i.Id == item.Id).IsDropped = true;
    }

    private void SetGameItems()
    {
        foreach (var scriptable in scriptableGameItems)
        {
            int index = scriptableGameItems.IndexOf(scriptable);
            _gameItems.Add(new GameItem(index, scriptable));
        }

        _deathItem = new GameItem(-1, deathItem);
        scriptableGameItems.Clear();
    }

    private void SetFrameColors()
    {
        foreach (var item in _gameItems)
        {
            if (item.Type == ItemType.BattleItem)
            {
                switch (item.Rarity)
                {
                    case Rarity.Common:
                        item.FrameColor = commonColor;
                        break;
                    
                    case Rarity.Rare:
                        item.FrameColor = rareColor;
                        break;
                    
                    case Rarity.Epic:
                        item.FrameColor = epicColor;
                        break;
                    
                    case Rarity.Legendary:
                        item.FrameColor = legendaryColor;
                        break;
                }
            }
        }
    }

    private void AddBombToItemsToSpawn()
    {
        if (GameSystem.GetZoneType(_system.CurrentLevelZone) == GameSystem.ZoneType.Normal)
        {
            int randomIndex = Random.Range(0, ItemsToSpawn.Count);
            ItemsToSpawn[randomIndex] = _deathItem;
        }
    }

    private void SetRandomItemsToSpawn(int amount)
    {
        ItemsToSpawn.Clear();
        List<GameItem> tempItems = new();

        for (int i = 0; i < amount; i++)
        {
            tempItems = SetValidItemsByRandomNumber(_gameItems);

            if (tempItems.Count == 0)
            {
                i--;
            }
            else
            {
                int randomIndex = Random.Range(0, tempItems.Count);
                ItemsToSpawn.Add(tempItems[randomIndex]);
                tempItems.Clear();
            }
        }
    }

    private List<GameItem> SetValidItemsByRandomNumber(List<GameItem> items)
    {
        List<GameItem> tempItems = new();
        float random = Random.Range(0, 1f);

        foreach (var item in items)
        {
            if (item.DropRate >= random)
            {
                bool isItemUnique = true;

                foreach (var itemToSpawn in ItemsToSpawn)
                {
                    if (item.Id == itemToSpawn.Id || item.IsDropped)
                    {
                        isItemUnique = false;
                        break;
                    }
                }

                if (isItemUnique)
                {
                    tempItems.Add(item);
                }
            }
        }

        return tempItems;
    }

    private void RemoveRepetitiveItems()
    {
        List<GameItem> clearedItems = _gameItems.ToList();

        foreach (var item in _gameItems)
        {
            int amount = 0;

            foreach (var copy in _gameItems)
            {
                if (item.Id == copy.Id)
                {
                    amount++;

                    if (amount > 2)
                    {
                        clearedItems.Remove(copy);
                    }
                }
            }
        }
        
        _gameItems = clearedItems;
    }

    private void ResetItems()
    {
        foreach (var item in _gameItems)
        {
            if (item.IsDropped)
            {
                item.IsDropped = false;
            }
        }
    }
}

using UnityEngine;

public class LootShowroomItemController : MonoBehaviour
{
    [Header("Resources")]
    [SerializeField] private Transform content;
    
    [Header("Values")]
    [SerializeField][Range(0.2f, 2f)] private float prefabScaleFactor;

    [Header("Dependencies")]
    [SerializeField] private WheelItemLarge wheelItemPrefab;

    private PlayerLoot _loot;
    
    private void OnEnable()
    {
        _loot = PlayerLoot.Instance;
        ShowLoot();
    }

    private void ShowLoot()
    {
        foreach (var item in _loot.PlayerItems)
        {
            var wheelItem = Instantiate(wheelItemPrefab, content);
            wheelItem.ShowStackAmount = true;
            wheelItem.Item = item;
            wheelItem.transform.localScale *= prefabScaleFactor;
            wheelItem.gameObject.SetActive(true);
        }
    }
}

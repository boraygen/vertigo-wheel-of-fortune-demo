using UnityEngine;

public class ZoneItemSpawner : MonoBehaviour
{
    [Header("Values")] 
    [SerializeField] private int zoneItemAmount;
    [SerializeField][Range(0.5f, 3f)] private float prefabScaleFactor = 1f;
    // [SerializeField][Range(0.5f, 2f)] private float fontSizeFactor = 1f;
    
    [Header("Dependencies")]
    [SerializeField] private ZoneItem prefab;
    [SerializeField] private Transform content;

    private GameSystem _events;
    
    private void Start()
    {
        _events = GameSystem.Instance;
        SpawnPrefabs();
    }

    private void SpawnPrefabs()
    {
        for (int i = 0; i < zoneItemAmount; i++)
        {
            var item = Instantiate(prefab, content);
            item.LevelNumber = i + 1;
            // item.ScaleFontSize(fontSizeFactor);
            item.transform.localScale *= prefabScaleFactor;
        }
        
        _events.CompleteSpawn();
    }
}

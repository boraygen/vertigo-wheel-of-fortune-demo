using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RestartButtonController : MonoBehaviour
{
    [Header("Dependencies")]
    [SerializeField] private Button button;

    private void Start()
    {
        button.onClick.AddListener(RestartMiniGame);        
    }

    private void RestartMiniGame()
    {
        int activeSceneIndex = SceneManager.GetActiveScene().buildIndex;
        SceneManager.LoadScene(activeSceneIndex);
    }
}
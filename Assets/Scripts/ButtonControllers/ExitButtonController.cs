using UnityEngine;
using UnityEngine.UI;

public class ExitButtonController : MonoBehaviour
{
    [Header("Dependencies")]
    [SerializeField] private GameObject lootShowroomPanel;
    [SerializeField] private Button button;

    private PlayerLoot _loot;
    
    private void Start()
    {
        _loot = PlayerLoot.Instance;
        button.onClick.AddListener(Execute);        
    }

    private void Execute()
    {
        if (_loot.PlayerItems.Count == 0)
        {
            Application.Quit();
        }
        else
        {
            lootShowroomPanel.SetActive(true);
        }
    }
}

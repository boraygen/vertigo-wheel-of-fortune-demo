﻿using UnityEngine;
using UnityEngine.UI;

public class SpinButtonController : MonoBehaviour
{
    [Header("Resources")] 
    [SerializeField] private Sprite silverBase;
    [SerializeField] private Sprite goldenBase;
    
    [Header("Dependencies")]
    [SerializeField] private Button button;

    private GameSystem _system;
    private Image _buttonOverlayImage;

    private void Start()
    {
        _system = GameSystem.Instance;
        button.onClick.AddListener(_system.StartWheelSpin);

        _buttonOverlayImage = button.transform.GetChild(0).GetComponent<Image>();
        
        _system.OnWheelSpinStarted += () => SetButtonActivation(false);
        _system.OnRewardInteractionEnded += () => SetButtonActivation(true);
        _system.OnRewardInteractionEnded += SetButtonBackground;
        SetButtonBackground();
    }

    private void SetButtonActivation(bool activation)
    {
        button.enabled = activation;
    }

    private void SetButtonBackground()
    {
        // set layout transparency -> color.a
        var zoneType = GameSystem.GetZoneType(_system.CurrentLevelZone);
        var color = _buttonOverlayImage.color;

        switch (zoneType)
        {
            case GameSystem.ZoneType.Silver:
                color.a = 0.18f;
                _buttonOverlayImage.sprite = silverBase;
                break;
            
            case GameSystem.ZoneType.Gold:
                color.a = 0.18f;
                _buttonOverlayImage.sprite = goldenBase;
                break;
            
            default:
                color.a = 0;
                break;
        }

        _buttonOverlayImage.color = color;
    }
}

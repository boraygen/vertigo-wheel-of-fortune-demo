﻿using UnityEngine ;
using UnityEngine.UI ;
using DG.Tweening ;
using UnityEngine.Events ;
using System.Collections.Generic ;

namespace EasyUI.PickerWheelUI
{
   public class PickerWheel : MonoBehaviour {

      [Header ("References :")]
      [SerializeField] private GameObject linePrefab;
      [SerializeField] private Transform linesParent;

      [Space]
      [SerializeField] private Transform pickerWheelTransform;
      [SerializeField] private Transform wheelCircle;
      [SerializeField] private GameObject wheelPiecePrefab;
      [SerializeField] private Transform wheelPiecesParent;

      [Space]
      [Header ("Sounds :")]
      [SerializeField] private AudioSource audioSource;
      [SerializeField] private AudioClip tickAudioClip;
      [SerializeField] [Range (0f, 1f)] private float volume = .5f;
      [SerializeField] [Range (-3f, 3f)] private float pitch = 1f;

      [Space]
      [Header ("Picker wheel settings :")]
      [Range (1, 20)] public int spinDuration = 8;
      [SerializeField] [Range (.2f, 2f)] private float wheelSize = 1f;

      [Space]
      [Header ("Picker wheel pieces :")]
      public WheelPiece[] wheelPieces;

      // Events
      private UnityAction _onSpinStartEvent ;
      private UnityAction<WheelPiece> _onSpinEndEvent;

      public bool IsSpinning { get; private set; }

      private Vector2 _pieceMinSize = new (81f, 146f);
      private Vector2 _pieceMaxSize = new (144f, 213f);
      private int _piecesMin = 2;
      private int _piecesMax = 12;

      private float _pieceAngle;
      private float _halfPieceAngle;
      private float _halfPieceAngleWithPaddings;

      private double _accumulatedWeight;
      private System.Random _rand = new System.Random();

      private List<int> _nonZeroChancesIndices = new();

      private void OnValidate()
      {
         if (pickerWheelTransform != null)
         {
            pickerWheelTransform.localScale = new Vector3 (wheelSize, wheelSize, 1f);
         }

         if (wheelPieces.Length > _piecesMax || wheelPieces.Length < _piecesMin)
         {
            Debug.LogError ("[PickerWheel] pieces length must be between " + _piecesMin + " and " + _piecesMax);
         }
      }
      
      private void Start()
      {
         _pieceAngle = 360f / wheelPieces.Length;
         _halfPieceAngle = _pieceAngle / 2f;
         _halfPieceAngleWithPaddings = _halfPieceAngle - (_halfPieceAngle / 4f);

         Generate();  

         CalculateWeightsAndIndices();
         if (_nonZeroChancesIndices.Count == 0)
         {
            Debug.LogError("You can't set all pieces chance to zero");
         }

         SetupAudio();
      }

      private void SetupAudio()
      {
         audioSource.clip = tickAudioClip;
         audioSource.volume = volume;
         audioSource.pitch = pitch;
      }

      private void Generate()
      {
         wheelPiecePrefab = InstantiatePiece();

         RectTransform rt = wheelPiecePrefab.transform.GetChild(0).GetComponent<RectTransform>();
         float pieceWidth = Mathf.Lerp(_pieceMinSize.x, _pieceMaxSize.x, 1f - Mathf.InverseLerp(_piecesMin, _piecesMax, wheelPieces.Length));
         float pieceHeight = Mathf.Lerp(_pieceMinSize.y, _pieceMaxSize.y, 1f - Mathf.InverseLerp(_piecesMin, _piecesMax, wheelPieces.Length));
         rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, pieceWidth);
         rt.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, pieceHeight);

         for (int i = 0; i < wheelPieces.Length; i++)
         {
            DrawPiece(i);
         }

         Destroy(wheelPiecePrefab);
      }

      private void DrawPiece(int index)
      {
         WheelPiece piece = wheelPieces[index];
         Transform pieceTrns = InstantiatePiece().transform.GetChild(0);

         pieceTrns.GetChild(0).GetComponent<Image>().sprite = piece.Icon;
         pieceTrns.GetChild(1).GetComponent<Text>().text = piece.Label;
         pieceTrns.GetChild(2).GetComponent<Text>().text = piece.Amount.ToString();

         //Line
         Transform lineTrns = Instantiate(linePrefab, linesParent.position, Quaternion.identity, linesParent).transform;
         lineTrns.RotateAround(wheelPiecesParent.position, Vector3.back, (_pieceAngle * index) + _halfPieceAngle);

         pieceTrns.RotateAround(wheelPiecesParent.position, Vector3.back, _pieceAngle * index);
      }

      private GameObject InstantiatePiece()
      {
         return Instantiate(wheelPiecePrefab, wheelPiecesParent.position, Quaternion.identity, wheelPiecesParent);
      }


      public void Spin()
      {
         if (IsSpinning) return;

         IsSpinning = true;

         _onSpinStartEvent?.Invoke();

         int index = GetRandomPieceIndex();
         WheelPiece piece = wheelPieces[index];

         if (piece.Chance == 0 && _nonZeroChancesIndices.Count != 0)
         {
            index = _nonZeroChancesIndices[Random.Range(0, _nonZeroChancesIndices.Count)];
            piece = wheelPieces [index];
         }

         float angle = -(_pieceAngle * index);

         float rightOffset = (angle - _halfPieceAngleWithPaddings) % 360;
         float leftOffset = (angle + _halfPieceAngleWithPaddings) % 360;

         float randomAngle = Random.Range(leftOffset, rightOffset);

         Vector3 targetRotation = Vector3.back * (randomAngle + 2 * 360 * spinDuration);

         //float prevAngle = wheelCircle.eulerAngles.z + halfPieceAngle;
         float prevAngle, currentAngle;
         prevAngle = currentAngle = wheelCircle.eulerAngles.z;

         bool isIndicatorOnTheLine = false;

         wheelCircle
            .DORotate(targetRotation, spinDuration, RotateMode.Fast)
            .SetEase(Ease.InOutQuart)
            .OnUpdate(() => {
               float diff = Mathf.Abs (prevAngle - currentAngle);
               if (diff >= _halfPieceAngle) 
               {
                  if (isIndicatorOnTheLine) 
                  {
                     audioSource.PlayOneShot (audioSource.clip);
                  }
                  prevAngle = currentAngle;
                  isIndicatorOnTheLine = !isIndicatorOnTheLine;
               }
               currentAngle = wheelCircle.eulerAngles.z;
            })
            .OnComplete(() => 
            {
               IsSpinning = false;
               _onSpinEndEvent?.Invoke (piece);

               _onSpinStartEvent = null; 
               _onSpinEndEvent = null;
            });
      }

      // private void FixedUpdate () {
      //
      // }

      public void OnSpinStart(UnityAction action) 
      {
         _onSpinStartEvent = action ;
      }

      public void OnSpinEnd(UnityAction<WheelPiece> action)
      {
         _onSpinEndEvent = action ;
      }


      private int GetRandomPieceIndex()
      {
         double r = _rand.NextDouble() * _accumulatedWeight;

         for (int i = 0; i < wheelPieces.Length; i++)
         {
            if (wheelPieces[i]._weight >= r)
            {
               return i ;
            }
         }

         return 0 ;
      }

      private void CalculateWeightsAndIndices()
      {
         for (int i = 0; i < wheelPieces.Length; i++)
         {
            WheelPiece piece = wheelPieces [ i ];

            //add weights:
            _accumulatedWeight += piece.Chance;
            piece._weight = _accumulatedWeight;

            //add index:
            piece.Index = i;

            //save non zero chance indices:
            if (piece.Chance > 0)
            {
               _nonZeroChancesIndices.Add(i);
            }
         }
      }
   }
}
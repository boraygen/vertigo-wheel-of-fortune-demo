using UnityEditor;
using UnityEngine;
using ItemType = ScriptableWheelItem.ItemType;

namespace Editor
{
    [CustomEditor(typeof(ScriptableWheelItem))]
    [CanEditMultipleObjects]
    public class ScriptableWheelItemInspector : UnityEditor.Editor
    {
        private SerializedProperty _amount;
        // private SerializedProperty _amountIncreasedPerLevelRatio;

        private void OnEnable()
        {
            _amount = serializedObject.FindProperty("amount");
            // _amountIncreasedPerLevelRatio = serializedObject.FindProperty("amountIncreasedPerLevelRatio");
        }

        public override void OnInspectorGUI()
        {
            ScriptableWheelItem item = (ScriptableWheelItem)target;

            // item name
            item.itemName = EditorGUILayout.TextField("Item Name", item.itemName);
            
            // sprite
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Sprite");
            item.sprite = (Sprite)EditorGUILayout.ObjectField(item.sprite, typeof(Sprite), allowSceneObjects: true);
            EditorGUILayout.EndHorizontal();

            // drop rate
            item.dropRate = EditorGUILayout.Slider(new GUIContent("(?) Drop Rate", "Higher value means higher drop chance"), item.dropRate, 0, 1f);

            // type
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.PrefixLabel("Type");
            item.type = (ItemType)EditorGUILayout.EnumPopup(item.type);
            EditorGUILayout.EndHorizontal();

            // values header
            if (item.type != ItemType.None)
            {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("Values", EditorStyles.boldLabel);
            }

            // values
            switch (item.type)
            {
                case ItemType.Expendable:
                    // amount
                    EditorGUILayout.PropertyField(
                        _amount, 
                        new GUIContent("(?) Amount", 
                            "Amount to be added to loot if dropped in Zone 1")
                        );
                    
                    // amount increase per level ratio
                    item.amountIncreasedPerLevelRatio = EditorGUILayout.Slider(
                        new GUIContent("(?) Amount Increase Ratio",
                            $"[Amount + (Amount * AmountIncreaseRatio * (ZoneLevel - 1))] rounded to integer will be added to loot if dropped\n\nExample {item.itemName} amount in Zone 3:\n{_amount.intValue} + ({_amount.intValue} * {item.amountIncreasedPerLevelRatio} * (3 - 1))"),
                        item.amountIncreasedPerLevelRatio,
                        0, 
                        1f
                    );
                    
                    // EditorGUILayout.PropertyField(
                    //     _amountIncreasedPerLevelRatio, 
                    //     new GUIContent("(?) Amount Increase Ratio",
                    //         $"[Amount + (Amount * AmountIncreaseRatio * (ZoneLevel - 1))] will be added to loot if dropped\n\nExample {item.itemName} amount in Zone 3:\n{_amount.intValue} + ({_amount.intValue} * {_amountIncreasedPerLevelRatio.floatValue} * (3 - 1))")
                    //     );
                    
                    // frame color
                    item.frameColor = EditorGUILayout.ColorField("Frame Color", item.frameColor);
                    break;
                
                case ItemType.BattleItem:
                    // rarity
                    EditorGUILayout.BeginHorizontal();
                    EditorGUILayout.PrefixLabel("Rarity");
                    item.rarity = (ScriptableWheelItem.Rarity)EditorGUILayout.EnumPopup(item.rarity);
                    EditorGUILayout.EndHorizontal();
                    
                    // weapon name
                    item.battleItemName = EditorGUILayout.TextField("Battle Item Name", item.battleItemName);
                    
                    // puzzle piece given
                    EditorGUI.BeginDisabledGroup(true);
                    item.puzzlePieceGiven = EditorGUILayout.IntField("Puzzle Piece Given", (int)item.rarity + 1);
                    EditorGUI.EndDisabledGroup();
                    break;
                
                case ItemType.Death:
                    // frame color
                    item.frameColor = EditorGUILayout.ColorField("Frame Color", item.frameColor);
                    break;
                
                // case ScriptableWheelItem.ItemType.Chest:
                //     EditorGUI.BeginDisabledGroup(true);
                //     item.amount = EditorGUILayout.IntField("Amount", 1);
                //     EditorGUI.EndDisabledGroup();
                //     break;
            }

            EditorUtility.SetDirty(target);
            serializedObject.ApplyModifiedProperties();
        }
    }
}
